Django views (RESTful API)
==========================

.. toctree::
        permissions
        services


.. automodule:: workflow_reg.views
   :members: