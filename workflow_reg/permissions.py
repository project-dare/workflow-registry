import json
import logging

import requests
from requests.exceptions import ConnectionError, ConnectTimeout
from rest_framework import permissions

from workflow_registry.settings import DARE_LOGIN_URL

logger = logging.getLogger(__name__)


class IsAuthenticated(permissions.IsAuthenticated):
    """
    Class that is used to implement the has_permission method. Extends the Django authentication class in order to
    implement external authentication from dare-login module.
    """

    def has_permission(self, request, view):
        """
        This method is called in the views that include the IsAuthenticated class in permissions. Performs a RESTful
        API call to the dare-login in order to authenticate the user.

        Args
            | request (HttpRequest): django Http request object containing all the provided information by the client.
            | view (ModelViewSet): the specific view that triggered the method.

        Returns
            bool: True/False, whether the user is successfully authenticated to the platform
        """
        if "docs" in request.build_absolute_uri():
            return True
        if type(request.data) != dict and not request.data._mutable:
            request.data._mutable = True
        if request.method == "POST" or request.method == "PUT" or request.method == "DELETE":
            token = request.data["access_token"]
        else:
            token = request.query_params["access_token"]
        if token:
            try:
                user_data = self.validate_token(token=token)
                if user_data[0] == 200:
                    user_data = json.loads(user_data[1])
                    request.data["username"] = user_data["username"]
                    request.data["user_id"] = user_data["user_id"]
                    request.data["issuer"] = user_data["issuer"]
                    return True
                else:
                    return False
            except (ConnectionError, ConnectTimeout, Exception) as e:
                logger.error("Could not authenticate user {}".format(e))
                return False
        else:
            logger.error("Token was not provided in the request's header")
            return False

    @staticmethod
    def validate_token(token):
        """
        Function to validate a given token. If the token is valid, the API asks for user info so as to retrieve a
        permanent id. Some of the returned fields (if token is valid) are as presented below:

        - "sub": "cfbdc618-1d1e-4b1d-9d35-3d23f271abe3",
        - "email_verified": true,
        - "name": "Sissy Themeli",
        - "preferred_username": "sthemeli",
        - "given_name": "Sissy",
        - "family_name": "Themeli",
        - "email": "sthemeli@iit.demokritos.gr"

        Args
            request: http request
            oidc: open-id client

        Returns
            str: user's permanent id
        """
        try:
            url = "{}/validate-token".format(DARE_LOGIN_URL)
            response = requests.post(url, data=json.dumps({"access_token": token}))
            return response.status_code, response.text
        except (ConnectionError, Exception) as e:
            print(e)
            return 500, "Could not validate token"
