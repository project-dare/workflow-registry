import json

import requests
from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User

from workflow_registry.settings import DARE_LOGIN_URL


class DareAuthBackend(BaseBackend):

    def authenticate(self, request, token=None, username=None, password=None):
        try:
            if token:
                return self.validate_token(token)
            elif username and password:
                data = {
                    "username": username,
                    "password": password,
                    "requested_issuer": "dare"
                }
                headers = {"Content-Type": "application/json"}
                r = requests.post(DARE_LOGIN_URL + '/auth', data=json.dumps(data), headers=headers)
                if r.status_code == 200:
                    r = json.loads(r.text)
                    return self.validate_token(r["access_token"], password)
            else:
                return None
        except (ConnectionError, Exception) as e:
            print(e)
            return None

    def get_user(self, user_id):
        try:
            user = User.objects.get(pk=user_id)
            return user
        except (User.DoesNotExist, BaseException, Exception):
            return None

    @staticmethod
    def validate_token(token, password=None):
        url = "{}/validate-token".format(DARE_LOGIN_URL)
        response = requests.post(url, data=json.dumps({"access_token": token, "full_resp": True}))
        if response.status_code == 200:
            response = json.loads(response.text)
            try:
                user = User.objects.get(username=response["username"])
                if password:
                    user.set_password(password)
                    user.save()
            except (User.DoesNotExist, Exception):
                username = response.get("username")
                email = response.get("email")
                given_name = response.get("given_name")
                family_name = response.get("family_name")
                if password:
                    user = User.objects.create_superuser(username=username, password=password, email=email,
                                                         is_active=True, first_name=given_name, last_name=family_name)
                else:
                    user = User.objects.create_superuser(username=username, email=email, is_active=True,
                                                         first_name=given_name, last_name=family_name)
                user.save()
            return user
        else:
            return None
