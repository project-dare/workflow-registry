import json
import logging

from django.contrib.auth.models import User
from django.core.exceptions import EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned
from django.db.utils import IntegrityError
from django.http import HttpResponse, JsonResponse
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from workflow_reg import utils
from workflow_reg.models import DockerEnv, DockerScript, Workflow, WorkflowPart
from workflow_reg.permissions import IsAuthenticated
from workflow_reg.serializers import DockerEnvSerializer, DockerScriptSerializer, WorkflowSerialiser, \
    WorkflowPartSerializer, UserSerializer
from workflow_reg.services import DockerEnvService, DockerScriptService, WorkflowService, WorkflowPartService

logger = logging.getLogger(__name__)


# Create your views here.
class DockerEnvView(viewsets.ModelViewSet):
    """
    View for the DockerEnv Django instance
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    serializer_class = DockerEnvSerializer
    queryset = DockerEnv.objects.all()

    def create(self, request, *args, **kwargs):
        """
        API endpoint, used to create a new DockerEnv instance, i.e. a docker container. It gives also the possibility
        to also register additional scripts used in the Dockerfile (e.g. entrypoint.sh).
        Usage: http://domain/workflow_registry/docker/      POST request
        Example json:

        {
            "docker_name": "name",
            "docker_tag": "tag",
            "script_names": ["script1.sh", "script2.sh"]
            "files": {
                "dockerfile": "bytes",
                "script1.sh": "bytes....",
                "script2.sh": "byteskjjgjgj"
            },
            "access_token": "token"
        }
        """
        try:
            user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
            dockerfile = request.data["files"]["dockerfile"]
            docker_name = request.data["docker_name"]
            docker_tag = request.data["docker_tag"]
            try:
                script_names = request.data["script_names"]
            except KeyError:
                script_names = {}
        except (KeyError, Exception) as e:
            msg = "Some parameters are missing from the request! {}".format(e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        try:
            scripts = utils.collect_scripts(script_names=script_names, files=request.data["files"])
            docker_env_service = DockerEnvService()
            docker_env = docker_env_service.create_docker_env(name=docker_name, tag=docker_tag, dockerfile=dockerfile,
                                                              user_identifier=user_identifier, scripts=scripts)
            serializer = DockerEnvSerializer(docker_env, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (IntegrityError, Exception) as e:
            msg = "Something went wrong while saving docker with name {} and tag {}! {}".format(docker_name, docker_tag,
                                                                                                e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["post"], detail=False, url_name="update_docker", url_path="update_docker")
    def update_docker(self, request, *args, **kwargs):
        """
        API endpoint to update an existing docker container
        POST request to http://domain/workflow-registry/docker/update_docker/

        Example json:

        {
            "docker_name": "name",
            "docker_tag" "tag",
            "update": {
                "tag": "v2.0"
            },
            "files": {
                "dockerfile": "gfgrfghrfgh"
            },
            "access_token": "token"
        }
        """
        user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
        docker_env_service = DockerEnvService()
        # check necessary params
        try:
            docker_fields_update = request.data["update"]
            docker_name = request.data["docker_name"]
            docker_tag = request.data["docker_tag"]
        except (KeyError, Exception):
            msg = "Please provide a docker name and tag and also fields to update"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        # check if file should be updated
        try:
            docker_file = request.data["files"]["dockerfile"]
        except (KeyError, Exception):
            docker_file = None

        try:
            docker_env = docker_env_service.update_docker_env(update_fields=docker_fields_update, name=docker_name,
                                                              tag=docker_tag, user_identifier=user_identifier,
                                                              docker_file=docker_file)
            serializer = DockerEnvSerializer(docker_env, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while updating docker with name {} and tag {}: {}".format(docker_name, docker_tag, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["post"], detail=False, url_name="provide_url", url_path="provide_url")
    def provide_url(self, request, *args, **kwargs):
        """
        API endpoint to update the URL of the docker image once built
        POST request to http://domain/workflow-registry/docker/provide_url/

        Example json:

        {
            "docker_name": "name",
            "docker_tag": "tag",
            "docker_url": "url",
            "access_token": "token"
        }
        """
        user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
        docker_env_service = DockerEnvService()
        try:
            docker_name = request.data["docker_name"]
            docker_tag = request.data["docker_tag"]
            docker_url = request.data["docker_url"]
        except (KeyError, Exception):
            msg = "Please provide a docker name, tag and url to the docker image"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            update_fields = {"url": docker_url}
            docker_env = docker_env_service.update_docker_env(update_fields=update_fields, name=docker_name,
                                                              tag=docker_tag, user_identifier=user_identifier)
            serializer = DockerEnvSerializer(docker_env, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while updating docker with name {} and tag {}: {}".format(docker_name, docker_tag, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["delete"], detail=False, url_name="delete_docker", url_path="delete_docker")
    def delete_docker(self, request, *args, **kwargs):
        """
        API endpoint to delete an existing docker container
        DELETE request to http://domain/workflow-registry/delete_docker/

        Example json:

        {
            "docker_name": "name",
            "docker_tag": "tag",
            "access_token": "token"
        }
        """
        docker_env_service = DockerEnvService()
        try:
            docker_name = request.data["docker_name"]
            docker_tag = request.data["docker_tag"]
        except(KeyError, Exception):
            msg = "Please provide a docker name and tag"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            docker_env_service.delete_docker_env(name=docker_name, tag=docker_tag)
            msg = "Docker env with name {} and tag {} is deleted".format(docker_name, docker_tag)
            return Response(msg, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while deleting docker with name {} and tag {}: {}".format(docker_name, docker_tag, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="by_name_tag", url_path="bynametag")
    def get_docker_by_name_and_tag(self, request, *args, **kwargs):
        """
        API endpoint to retrieve a unique docker container
        GET request to http://domain/workflow-registry/docker/bynametag?docker_name=name&docker_tag=tag
                                                                        &access_token=token
        """
        docker_env_service = DockerEnvService()
        try:
            docker_name = request.query_params["docker_name"]
            docker_tag = request.query_params["docker_tag"]
        except(KeyError, Exception):
            msg = "Please provide a docker name and tag"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            docker_env = docker_env_service.get_unique_docker_env(name=docker_name, tag=docker_tag)
            serializer = DockerEnvSerializer(docker_env, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving docker with name {} and tag {}: {}".format(docker_name, docker_tag, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="by_user", url_path="byuser")
    def get_docker_by_user(self, request, *args, **kwargs):
        """
        API endpoint to retrieve all dockers from a specific user
        GET request to http://domain/workflow-registry/docker/byuser?requested_user=username&access_token=token
        """
        docker_env_service = DockerEnvService()
        user = request.query_params["requested_user"] if "requested_user" in request.query_params.keys() else \
            request.data["username"]
        user_identifier = ''.join([user, utils.user_identifier_sep, request.data["issuer"]])
        try:
            docker_env = docker_env_service.filter_docker_env_by_user(user_identifier=user_identifier)
            serializer = DockerEnvSerializer(docker_env, many=True, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving docker environments of user {}: {}".format(user, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="download", url_path="download")
    def download_dockerfile_and_scripts(self, request, *args, **kwargs):
        """
        API endpoint to download a docker container, i.e. the Dockerfile and the relevant scripts
        GET request to http://domain/workflow-registry/docker/download?docker_name=name&docker_tag=tag
                                                                        &access_token=token
        """
        docker_env_service = DockerEnvService()
        try:
            docker_name = request.query_params["docker_name"]
            docker_tag = request.query_params["docker_tag"]
        except(KeyError, Exception):
            msg = "Please provide a docker name and tag"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            zipped_file = docker_env_service.download_docker_env(name=docker_name, tag=docker_tag)
            response = HttpResponse(zipped_file, content_type='application/octet-stream')
            response['Content-Disposition'] = 'attachment; filename=docker_env_{}_{}.zip'.format(docker_name,
                                                                                                 docker_tag)
            return response
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving docker with name {} and tag {}: {}".format(docker_name, docker_tag, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class DockerScriptView(viewsets.ModelViewSet):
    """
    API View for DockerScript Django Model
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    serializer_class = DockerScriptSerializer
    queryset = DockerScript.objects.all()

    @action(methods=["post"], detail=False, url_name="add", url_path="add")
    def add_script(self, request, *args, **kwargs):
        """
        API endpoint to add a new script to an existing DockerEnv instance
        POST request to http://domain/workflow-registry/scripts/add/

        Example json:

        {
            "docker_name": "name",
            "docker_tag": "tag",
            "script_name": "entrypoint.sh",
            "files": {
                "entrypoint.sh": b"fgfghfhgfgh"
            },
            "access_token": "token"
        }
        """
        user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
        docker_script_service = DockerScriptService()
        try:
            docker_name = request.data["docker_name"]
            docker_tag = request.data["docker_tag"]
            script_name = request.data["script_name"]
            script_content = request.data["files"][script_name]
        except(KeyError, Exception):
            msg = "Please provide a docker name, tag and script details"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            script = docker_script_service.create_script(name=script_name, docker_name=docker_name,
                                                         docker_tag=docker_tag, script=script_content,
                                                         user_identifier=user_identifier)
            serializer = DockerScriptSerializer(script, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving docker with name {} and tag {}: {}".format(docker_name, docker_tag, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["post"], detail=False, url_name="edit", url_path="edit")
    def edit_script(self, request, *args, **kwargs):
        """
        API endpoint to update an existing script associated to an existing DockerEnv instance
        POST request to http://domain/workflow-registry/scripts/edit/

        Example json:

        {
            "docker_name": "name",
            "docker_tag": "tag",
            "script_name": "entrypoint.sh",
            "files": {
                "entrypoint.sh": b"fgfghfhgfgh"
            },
            "access_token": "token"
        }
        """
        user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
        docker_script_service = DockerScriptService()
        try:
            docker_name = request.data["docker_name"]
            docker_tag = request.data["docker_tag"]
            script_name = request.data["script_name"]
            script_content = request.data["files"][script_name]
        except(KeyError, Exception):
            msg = "Please provide a docker name, tag and script details"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            script = docker_script_service.update_script(name=script_name, docker_name=docker_name,
                                                         docker_tag=docker_tag, script_content=script_content,
                                                         user_identifier=user_identifier)
            serializer = DockerScriptSerializer(script, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving docker with name {} and tag {}: {}".format(docker_name, docker_tag, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["delete"], detail=False, url_name="delete", url_path="delete")
    def delete_script(self, request, *args, **kwargs):
        """
        API endpoint to delete an existing docker script
        DELETE request to http://domain/workflow-registry/scripts/delete/

        Example json:

        {
            "docker_name": "name",
            "docker_tag": "tag",
            "script_name": "entrypoint.sh",
            "access_token": "token"
        }
        """
        docker_script_service = DockerScriptService()
        try:
            docker_name = request.data["docker_name"]
            docker_tag = request.data["docker_tag"]
            script_name = request.data["script_name"]
        except(KeyError, Exception):
            msg = "Please provide a docker name, tag and script details"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            docker_script_service.delete_script(name=script_name, docker_name=docker_name, docker_tag=docker_tag)
            msg = "Script with name {} for docker with name {} and tag {} is deleted".format(script_name, docker_name,
                                                                                             docker_tag)
            return Response(msg, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while deleting script with name {} for docker with name {} and tag {}: {}".format(script_name,
                                                                                                           docker_name,
                                                                                                           docker_tag,
                                                                                                           e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="download", url_path="download")
    def download_script(self, request, *args, **kwargs):
        """
        API endpoint to download a specific script
        GET request to http://domain/workflow-registry/scripts/download?docker_name=name&docker_tag=tag
                                                                        &script_name=scriptname&access_token=token
        """
        docker_script_service = DockerScriptService()
        try:
            docker_name = request.query_params["docker_name"]
            docker_tag = request.query_params["docker_tag"]
            script_name = request.query_params["script_name"]
        except(KeyError, Exception):
            msg = "Please provide a docker name, tag and script details"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        script = docker_script_service.get_unique_script(docker_name=docker_name, docker_tag=docker_tag,
                                                         script_name=script_name)
        response = HttpResponse(script.script)
        response['Content-Type'] = 'text/plain'
        response['Content-Disposition'] = 'attachment; filename={}'.format(script_name)
        return response

    @action(methods=["get"], detail=False, url_name="by_name", url_path="byname")
    def get_script_by_name(self, request, *args, **kwargs):
        """
        API endpoint to retrieve a unique script based on a docker container
        GET endpoint to http://domain/workflow-registry/scripts/byname?docker_name=name&docker_tag=tag
                                                                        &script_name=scriptname&access_token=token
        """
        docker_script_service = DockerScriptService()
        try:
            docker_name = request.query_params["docker_name"]
            docker_tag = request.query_params["docker_tag"]
            script_name = request.query_params["script_name"]
        except(KeyError, Exception):
            msg = "Please provide a docker name, tag and script details"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            script = docker_script_service.get_unique_script(docker_name=docker_name, docker_tag=docker_tag,
                                                             script_name=script_name)
            serializer = DockerScriptSerializer(script, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving script with name {} for docker with name {} and tag {}: {}".format(
                script_name, docker_name, docker_tag, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class WorkflowView(viewsets.ModelViewSet):
    """
    API View for Workflow Django Model, which represents a CWL workflow
    """

    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    serializer_class = WorkflowSerialiser
    queryset = Workflow.objects.all()

    def create(self, request, *args, **kwargs):
        """
        API endpoint to create a new workflow. The user can register the workflow parts as well
        POST endpoint to http://domain/workflow-registry/workflows/

        Example json

        {
            "workflow_name": "demo_workflow.cwl",
            "workflow_version": "v1.0",
            "spec_file_name": "spec.yaml",
            "docker_name": "name",
            "docker_tag": "tag",
            "workflow_part_data":[
            {
                "name": "arguments.cwl",
                "version": "v1.0",
                "spec_name": "arguments.yaml"
            },
            {
                "name": "tar_param.cwl",
                "version": "v1.0",
                "spec_name": "tar_param.yaml"
            }
            ],
            "files": {
                "demo_workflow.cwl": "fgfghfghf",
                "spec.yaml": "fgfgfgrgrwet",
                "arguments.cwl": "bytes",
                "arguments.yaml": "ejhrthjro",
                "tar_param.cwl": "hjgfruio",
                "tar_param.yaml": "fgriooeroe"
            },
            "access_token": "token"
        }
        """
        user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
        workflow_service = WorkflowService()
        try:
            workflow_name = request.data["workflow_name"]
            workflow_version = request.data["workflow_version"]
            spec_file_name = request.data["spec_file_name"]
            workflow_file = request.data["files"]["workflow_file"]
            spec_file = request.data["files"]["spec_file"]
            docker_name = request.data["docker_name"]
            docker_tag = request.data["docker_tag"]
            try:
                workflow_part_data = request.data["workflow_part_data"]
                workflow_parts = {}
                if workflow_part_data:
                    for workflow_part_dat in workflow_part_data:
                        name = workflow_part_dat["name"]
                        version = workflow_part_dat["version"]
                        spec_name = workflow_part_dat["spec_name"] if "spec_name" in workflow_part_dat.keys() else None
                        workflow_part = request.data["files"][name]
                        workflow_data = {"name": name, "version": version, "workflow_file": workflow_part}
                        if spec_name:
                            spec_file_part = request.data["files"][spec_name]
                            workflow_data["spec_name"] = spec_name
                            workflow_data["spec_file"] = spec_file_part
                        workflow_parts[name] = workflow_data
            except KeyError:
                workflow_parts = {}
        except (KeyError, Exception) as e:
            msg = "Missing request data! {}".format(e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            workflow = workflow_service.create_workflow(workflow_name=workflow_name, workflow_version=workflow_version,
                                                        spec_file_name=spec_file_name, workflow_file=workflow_file,
                                                        spec_file=spec_file, docker_name=docker_name,
                                                        docker_tag=docker_tag, user_identifier=user_identifier,
                                                        workflow_parts=workflow_parts)
            serializer = WorkflowSerialiser(workflow, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (IntegrityError, Exception) as e:
            msg = "Error while creating a workflow with name {} and version {}! {}".format(workflow_name,
                                                                                           workflow_version, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["post"], detail=False, url_name="update_workflow", url_path="update_workflow")
    def update_workflow(self, request, *args, **kwargs):
        """
        API endpoint to update an existing workflow.
        POST endpoint to http://domain/workflow-registry/workflows/update_workflow/

        Example json

        {
            "workflow_name": "demo_workflow.cwl",
            "workflow_version": "v1.0",
            "files": {
                "workflow_file": "fgfghfghf",
                "spec_file": "fgfgfgrgrwet",
            },
            "update": {
                "version": "v1.1"
            }
            "access_token": "token"
        }
        """
        user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
        workflow_service = WorkflowService()
        try:
            workflow_fields_update = request.data["update"]
            workflow_name = request.data["workflow_name"]
            workflow_version = request.data["workflow_version"]
            try:
                workflow_file = request.data["files"]["workflow_file"]
            except KeyError:
                workflow_file = None
            try:
                spec_file = request.data["files"]["spec_file"]
            except KeyError:
                spec_file = None
        except (KeyError, Exception):
            msg = "Please provide a workflow name and version and also fields to update"
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            workflow = workflow_service.update_workflow(name=workflow_name, version=workflow_version,
                                                        workflow_file=workflow_file, spec_file=spec_file,
                                                        user_identifier=user_identifier,
                                                        workflow_fields_update=workflow_fields_update)
            serializer = WorkflowSerialiser(workflow, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while updating workflow with name {} and version {}: {}".format(workflow_name,
                                                                                         workflow_version, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["post"], detail=False, url_name="update_docker", url_path="update_docker")
    def update_docker(self, request, *args, **kwargs):
        """
            API endpoint to update the docker env associated with a specific workflow
            POST endpoint to http://domain/workflow-registry/workflows/update_docker/

        Example json

        {
            "workflow_name": "demo_workflow.cwl",
            "workflow_version": "v1.0",
            "docker_name": "test",
            "docker_tag": "v1.0",
            "access_token": "token"
        }
        """
        user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
        workflow_service = WorkflowService()
        try:
            workflow_name = request.data["workflow_name"]
            workflow_version = request.data["workflow_version"]
            docker_name = request.data["docker_name"]
            docker_tag = request.data["docker_tag"]
        except (KeyError, Exception) as e:
            msg = "Missing name, version or docker! {}".format(e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            workflow = workflow_service.update_docker(workflow_name=workflow_name, workflow_version=workflow_version,
                                                      docker_name=docker_name, docker_tag=docker_tag,
                                                      user_identifier=user_identifier)
            serializer = WorkflowSerialiser(workflow, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while updating workflow with name {} and version {}: {}".format(workflow_name,
                                                                                         workflow_version, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["delete"], detail=False, url_name="delete_workflow", url_path="delete_workflow")
    def delete_workflow(self, request, *args, **kwargs):
        """
        API endpoint to delete an existing workflow.
        DELETE endpoint to http://domain/workflow-registry/workflows/delete_workflow/

        Example json

        {
            "workflow_name": "demo_workflow.cwl",
            "workflow_version": "v1.0",
            "access_token": "token"
        }
        """
        workflow_service = WorkflowService()
        try:
            workflow_name = request.data["workflow_name"]
            workflow_version = request.data["workflow_version"]
        except (KeyError, Exception) as e:
            msg = "Missing name and version! {}".format(e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            workflow_service.delete_workflow(name=workflow_name, version=workflow_version)
            msg = "Workflow with name {} and version {} is deleted".format(workflow_name, workflow_version)
            return Response(msg, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while deleting workflow with name {} and version {}: {}".format(workflow_name,
                                                                                         workflow_version, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="by_name_version", url_path="bynameversion")
    def get_workflow_by_name_and_version(self, request, *args, **kwargs):
        """
        API endpoint to get unique workflow
        GET endpoint to http://domain/workflow-registry/workflows/bynameversion?workflow_name=name
                                                                &workflow_version=version&access_token=token
        """
        workflow_service = WorkflowService()
        try:
            workflow_name = request.query_params["workflow_name"]
            workflow_version = request.query_params["workflow_version"]
        except (KeyError, Exception) as e:
            msg = "Missing name and version! {}".format(e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            workflow = workflow_service.get_unique_workflow(name=workflow_name, version=workflow_version)
            serializer = WorkflowSerialiser(workflow, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Workflow with name {} and version {} not found! {}".format(workflow_name, workflow_version, e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="download", url_path="download")
    def download_workflow(self, request, *args, **kwargs):
        """
        API endpoint to download a workflow
        GET request to http://domain/workflow-registry/donwload?workflow_name=name&workflow_version=version
                                                                &dockerized=True&access_token=token
        """
        workflow_service = WorkflowService()
        try:
            workflow_name = request.query_params["workflow_name"]
            workflow_version = request.query_params["workflow_version"]
            try:
                dockerized = request.query_params["dockerized"]
            except KeyError:
                dockerized = None
        except (KeyError, Exception) as e:
            msg = "Missing name and version! {}".format(e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            zipped_file = workflow_service.download_workflow(name=workflow_name, version=workflow_version,
                                                             dockerized=dockerized)
            response = HttpResponse(zipped_file, content_type='application/octet-stream')
            response['Content-Disposition'] = 'attachment; filename=workflow_{}_{}.zip'.format(workflow_name,
                                                                                               workflow_version)
            return response
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Workflow with name {} and version {} not found! {}".format(workflow_name, workflow_version, e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class WorkflowPartView(viewsets.ModelViewSet):
    """
    API View for WorkflowPart Django model
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = "id"
    serializer_class = WorkflowPartSerializer
    queryset = WorkflowPart.objects.all()

    @action(methods=["post"], detail=False, url_name="add", url_path="add")
    def add_workflow_part(self, request, *args, **kwargs):
        """
        API endpoint to create a new workflow part, associated with a CWL workflow
        POST request to http://domain/workflow-registry/workflow_parts/add/

        Example json

        {
            "workflow_name": "demo_workflow.cwl",
            "workflow_version": "v1.0",
            "workflow_part_name": "arguments.cwl",
            "workflow_part_version": "v1.0",
            "spec_name": "arguments.yaml",
            "files": {
                "arguments.cwl": "grfgtrte",
                "arguments.yaml": "weojfodoj"
            },
            "access_token": "token"
        }
        """
        workflow_part_service = WorkflowPartService()
        user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
        try:
            workflow_name = request.data["workflow_name"]
            workflow_version = request.data["workflow_version"]
            workflow_part_name = request.data["workflow_part_name"]
            workflow_part_version = request.data["workflow_part_version"]
            spec_name = request.data["spec_name"] if "spec_name" in request.data.keys() else None
            workflow_part_file = request.data["files"][workflow_part_name]
        except (KeyError, Exception) as e:
            msg = "Missing data from request! {}".format(e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            if spec_name:
                spec = request.data["files"][spec_name]
                workflow_part = workflow_part_service.create_workflow_part(workflow_part_name, workflow_part_version,
                                                                           workflow_part_file, workflow_name,
                                                                           workflow_version, user_identifier, spec_name,
                                                                           spec)
            else:
                workflow_part = workflow_part_service.create_workflow_part(workflow_part_name, workflow_part_version,
                                                                           workflow_part_file, workflow_name,
                                                                           workflow_version, user_identifier)
            serializer = WorkflowPartSerializer(workflow_part, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (IntegrityError, EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while creating workflow part with name {} and version {} for workflow with name {}! {}".format(
                workflow_part_name, workflow_part_version, workflow_name, e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["post"], detail=False, url_name="edit", url_path="edit")
    def update_workflow_part(self, request, *args, **kwargs):
        """
        API endpoint to update an existing workflow part, associated with a CWL workflow
        POST request to http://domain/workflow-registry/workflow_parts/edit/

        Example json

        {
            "workflow_name": "demo_workflow.cwl",
            "workflow_version": "v1.0",
            "workflow_part_name": "arguments.cwl",
            "workflow_part_version": "v1.0",
            "spec_name": "arguments.yaml",
            "files": {
                "arguments.cwl": b"grfgtrte",
                "arguments.yaml": b"weojfodoj"
            },
            "update": {
                "version": "v1.1"
            }
            "access_token": "token"
        }
        """
        user_identifier = ''.join([request.data["username"], utils.user_identifier_sep, request.data["issuer"]])
        workflow_part_service = WorkflowPartService()
        try:
            workflow_fields_update = request.data["update"]
            workflow_name = request.data["workflow_name"]
            workflow_version = request.data["workflow_version"]
            workflow_part_name = request.data["workflow_part_name"]
            workflow_part_version = request.data["workflow_part_version"]
            spec_name = request.data["spec_name"] if "spec_name" in request.data.keys() else None
        except (KeyError, Exception):
            msg = "Please provide a workflow name and version and also fields to update"
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            workflow_file = request.data["files"][workflow_part_name]
        except (KeyError, Exception):
            workflow_file = None
        try:
            spec = request.data["files"][spec_name] if spec_name else None
        except (KeyError, Exception):
            spec = None
        try:
            workflow_part = workflow_part_service.update_workflow_part(workflow_name, workflow_version,
                                                                       workflow_part_name, workflow_part_version,
                                                                       workflow_fields_update, user_identifier,
                                                                       workflow_file, spec)
            serializer = WorkflowPartSerializer(workflow_part, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while updating workflow with name {} and version {}: {}".format(workflow_name,
                                                                                         workflow_version, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["delete"], detail=False, url_name="delete", url_path="delete")
    def delete_workflow_part(self, request, *args, **kwargs):
        """
        API endpoint to delete an existing workflow part, associated with a CWL workflow
        POST request to http://domain/workflow-registry/workflow_parts/delete/

        Example json

        {
            "workflow_name": "demo_workflow.cwl",
            "workflow_version": "v1.0",
            "workflow_part_name": "arguments.cwl",
            "workflow_part_version": "v1.0",
            "access_token": "token"
        }
        """
        workflow_part_service = WorkflowPartService()
        try:
            workflow_name = request.data["workflow_name"]
            workflow_version = request.data["workflow_version"]
            workflow_part_name = request.data["workflow_part_name"]
            workflow_part_version = request.data["workflow_part_version"]
        except (KeyError, Exception) as e:
            msg = "Missing data! {}".format(e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            workflow_part_service.delete_workflow_part(workflow_name=workflow_name, workflow_version=workflow_version,
                                                       workflow_part_name=workflow_part_name,
                                                       workflow_part_version=workflow_part_version)
            msg = "Workflow with name {} and version {} is deleted".format(workflow_name, workflow_version)
            return Response(msg, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while deleting workflow part with name {} and version {}! {}".format(workflow_name,
                                                                                              workflow_version, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="by_name_version", url_path="bynameversion")
    def get_workflow_part_by_name_and_version(self, request, *args, **kwargs):
        """
        API endpoint to get a unique workflow part
        GET request to http://domain/workflow-registry/workflow_parts/bynameversion?workflow_name=workflow_name&
                                                                    workflow_version=workflow_version&
                                                                    workflow_part_name=workflow_part_name&
                                                                    workflow_part_version=workflow_part_version&
                                                                    access_token=token
        """
        workflow_part_service = WorkflowPartService()
        try:
            workflow_name = request.query_params["workflow_name"]
            workflow_version = request.query_params["workflow_version"]
            workflow_part_name = request.query_params["workflow_part_name"]
            workflow_part_version = request.query_params["workflow_part_version"]
        except (KeyError, Exception) as e:
            msg = "Missing data! {}".format(e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            workflow_part = workflow_part_service.get_unique_workflow_part(workflow_name=workflow_name,
                                                                           workflow_version=workflow_version,
                                                                           workflow_part_name=workflow_part_name,
                                                                           workflow_part_version=workflow_part_version)
            serializer = WorkflowPartSerializer(workflow_part, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Workflow part with name {} and version {} not found! {}".format(workflow_part_name,
                                                                                   workflow_part_version, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="download", url_path="download")
    def download_workflow_part(self, request, *args, **kwargs):
        """
        API endpoint to download a specific workflow part
        GET request to http://domain/workflow-registry/workflow_parts/download?workflow_name=workflow_name&
                                                                    workflow_version=workflow_version&
                                                                    workflow_part_name=workflow_part_name&
                                                                    workflow_part_version=workflow_part_version&
                                                                    access_token=token
        """
        workflow_part_service = WorkflowPartService()
        try:
            workflow_name = request.query_params["workflow_name"]
            workflow_version = request.query_params["workflow_version"]
            workflow_part_name = request.query_params["workflow_part_name"]
            workflow_part_version = request.query_params["workflow_part_version"]
        except(KeyError, Exception):
            msg = "Please provide a docker name, tag and script details"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        zipped_file = workflow_part_service.download_workflow_part(workflow_name=workflow_name,
                                                                   workflow_version=workflow_version,
                                                                   workflow_part_name=workflow_part_name,
                                                                   workflow_part_version=workflow_part_version)
        response = HttpResponse(zipped_file, content_type='application/octet-stream')
        response['Content-Disposition'] = 'attachment; filename=workflow_{}_{}.zip'.format(workflow_part_name,
                                                                                           workflow_part_version)
        return response


class LoginView(viewsets.GenericViewSet):
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer

    @action(methods=["POST"], detail=False, url_name="keycloak_user", url_path="keycloak_user")
    def init_keycloak_user(self, request, *args, **kwargs):
        username = request.data["username"]
        password = request.data["password"]
        token = request.data["access_token"]
        email = request.data["email"]
        given_name = request.data["given_name"]
        family_name = request.data["family_name"]
        try:
            u = User.objects.get(username=username)
            print(u)
        except (IntegrityError, Exception):
            print("User {} not found! Creating user".format(username))
            try:
                u = User.objects.create_superuser(username=username, password=password, email=email,
                                                  first_name=given_name, last_name=family_name)
                u.save()
                print("User {} is saved".format(username))
            except (IntegrityError, Exception) as e:
                msg = "An error occurred while saving user {}".format(e)
                return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return JsonResponse(json.dumps({"access_token": token}), safe=False, status=status.HTTP_200_OK)
