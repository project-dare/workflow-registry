from django.apps import AppConfig


class WorkflowRegConfig(AppConfig):
    name = 'workflow_reg'
    verbose_name = "Workflow Registry"
