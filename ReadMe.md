# Workflow-registry

This component is a Django Web Service with a MySQL database in the backend. It enables the registration of CWL workflows followed by a docker container, i.e. the necessary environment for the workflow execution. Thus, the user can register docker containers, i.e. a Dockerfile with relevant scripts e.g. entrypoint.sh to build the environment he/she needs. Additionally, the user can register CWL workflows, i.e. CWL files characterized as workflows as well as workflow parts (i.e. CWL files characterized as CommandLineTool) which should be associated to a docker container. 

Workflow registry is used by the DARE Execution API to enable CWL execution. Based on the requested registered workflow, the DARE API uses the specified docker container to execute the job.

## Documentation

sphinx-apidoc -f --ext-autodoc --ext-viewcode --ext-coverage -o source/ ../